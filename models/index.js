'use strict';

var Sequelize = require('sequelize')
	, sequelize
	, db;

exports.init = function(app) {
	db = app.get('config').db;
	sequelize = new Sequelize(db.database, db.username, db.password, db.options);
	app.set('sequelize', sequelize);
	app.set('db', this.registerAndGetModels(sequelize));
	sequelize.sync().success(function() {
	}).error(function(err) {
		console.log(err);
	});
};

exports.registerAndGetModels = function(sequelize) {
	var User = sequelize.import(__dirname + '/users');
	var Profile = sequelize.import(__dirname + '/profiles');
	var Room = sequelize.import(__dirname + '/rooms');
	//var User_Room = sequelize.import(__dirname + '/user_room');

	// User_Room.hasMany(User, {foreignKey: 'user_id'});
	///Room.belongsToMany(User, {through: 'user_room'});
	Room.hasMany(User, {constraints: false});
	Room.belongsTo(User, {foreignKey: 'host_id'});
	Profile.belongsTo(User, {foreignKey: 'user_id'});
	//Room.hasOne(User, {foreignKey: 'room_id'});
	///User.hasOne(Room, {foreignKey: 'host_id'});
	

	return {
		User: User,
		Profile: Profile,
		Room: Room
	};
};
