module.exports = function(sequelize, DataTypes) {
	return sequelize.define('profiles', {
		image_url: {
			type: DataTypes.STRING,
			allowNull: false
		}
	}, {
		freezeTableName: true,
		tableName: 'profiles'
	});
}
