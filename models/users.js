var crypto = require('crypto');

var encryptPassword = function(password) {
	return crypto.createHmac('sha1', 'm_go').update(password).digest('hex');
};

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('users', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},

		email: {
			type: DataTypes.STRING,
			allowNull: true,
			unique: true
		},

		password: {
			type: DataTypes.STRING,
			allowNull: true
		},

		token: {
			type: DataTypes.STRING,
			allowNull: true
		},

		name: {
			type: DataTypes.STRING,
			allowNull: false
		},

		age: {
			type: DataTypes.INTEGER(2),
			allowNull: true
		},

		gender: {
			type: DataTypes.ENUM('m', 'f'),
			defaultValue: 'm',
			allowNull: false
		},

		is_virtual: {
			type: DataTypes.BOOLEAN,
		}
	}, {
		freezeTableName: true,
		tableName: 'users',

		setterMethods: {
			password: function(password) {
				return this.setDataValue('password', encryptPassword(password));
			}
		},

		instanceMethods: {
			authenticate: function(plainText) {
				return encryptPassword(plainText) === this.password;
			}
		},

		validate: {
			ifNotVirtualUser: function() {
				if (this.is_virtual === null) {
					if ((this.email === null) || (this.password === null) || (this.token === null)) {
						throw new Error("가상유저가 아닌 경우 email, password, token이 필요합니다");
					}
				}
			}
		}
	})
};
