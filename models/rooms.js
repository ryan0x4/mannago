module.exports = function(sequelize, DataTypes) {
	return sequelize.define('rooms', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},

		title: {
			type: DataTypes.STRING,
			allowNull: false
		},

		description: {
			type: DataTypes.STRING,
			allowNull: true
		},

		photo_url: {
			type: DataTypes.STRING,
			allowNull: true
		},

		region: {
			type: DataTypes.ENUM,
			values: ['hongdae', 'gundae', 'gangnam'],
			allowNull: true
		},

		size: {
			type: DataTypes.INTEGER(1),
			defaultValue: 0,
			allowNull: true
		},

		is_end: DataTypes.BOOLEAN
	}, {
		freezeTableName: true,
		tableName: 'rooms'
	});
}
