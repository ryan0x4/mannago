/* logger.error()을 console.log()로 바꿨음. 차후 logger 도입 후 다시 변경해야. logger.info는 그냥 놔뒀음 */
var crypto = require('crypto')
	, path = require('path')
	, fs = require('fs')
	, validator = require('validator')
	, _ = require('underscore')
	, sequelize
	, User
	, Room
	, Profile
	, handlers;

exports.init = function(app) {
	sequelize = app.get('sequelize');
	User = app.get('db').User;
	Room = app.get('db').Room;
	Profile = app.get('db').Profile;

	/* API */
	app.post('/users', handlers.createUsers); // SignUp
	app.post('/tokens', handlers.createTokens); // SingIn
	app.get('/users/user/:user_id', handlers.getUser); // 유저 조회
	app.get('/users/count', handlers.getUsersCount); //  접속중인 유저 수 조회
	app.post('/users/:user_id/delete', handlers.deleteUser); // 유저 삭제
	app.post('/users/:user_id/update', handlers.updateUser); // 유저 프로필 수정
	app.post('/rooms', handlers.createRoom); // 방 생성
	app.get('/rooms/:region', handlers.getRooms); // 해당 지역 전체 방 조회
	app.get('/rooms/room/:room_id', handlers.getRoom); // 특정 방 조회
	app.get('/rooms/count', handlers.getTotalRoomsCount); // 전체 방 갯수 조회
	app.post('/rooms/delete', handlers.deleteRoom); // 방 삭제
	app.post('/rooms/update', handlers.updateRoom); // 방 수정
	app.get('/files/:filename', handlers.downloadFile); // 프로필 사진 다운로드

	app.get('/events', handlers.events);
	/* web */
	app.get('/', handlers.index);
};

function sendError(res, errMsg) {
	res.send(200, {
		result: 0,
		msg: errMsg
	});
};

function getLogFormat(req) {
	return req.ip + ' - - "' + req.method + ' ' + req.path + '" ';
}

exports.handlers = handlers = {
	createUsers: function(req, res) {
		var email = req.body.email
			, userName = req.body.username
			, password = req.body.password
			, age = req.body.age
			, gender = req.body.gender;

		if (!validator.isEmail(email) || validator.isNull(userName) || validator.isNull(password)) {
			console.log(getLogFormat(req) + '잘못된 요청 / email: ' + email);
			sendError(res, '잘못된 요청입니다');
			return;
		}

		User.find({ where: { email: email } }).success(function(user) {
			if (user) {
				console.log(getLogFormat(req) + '유저 계정 생성 실패 / email: ' + email);
				sendError(res, '이메일이 존재합니다. 해당 이메일로 로그인 하시거나 다른 이메일로 가입 해주세요.');
			} else {
				var token = crypto
				.createHash('md5')
				.update(email + (new Date()).getTime() + 'vobble')
				.digest('hex');

				var userData = {
					email: email,
					password: password,
					token: token,
					name: userName,
					age: age,
					gender: gender,
				};

				User.create(userData).success(function(user) {
					//logger.info(getLogFormat(req) + '유저 생성 성공 / user_id: ' + user.values.user_id);
					res.send(200, { result: 1, user_id: user.values.id });
				}).error(function(err) {
					console.log(getLogFormat(req) + '유저 생성 실패 Sequelize 오류 / email: ' + email);
					console.log(err);
					sendError(res, '서버 오류');
				});
			}
		}).error(function(err) {
			console.log(getLogFormat(req) + '유저 조회 실패 Sequelize 오류 / email: ' + email);
			console.log(err);
			sendError(res, '서버 오류');
		});
	},

	createTokens: function(req, res) {
		var email = req.body.email
			, password = req.body.password;

		if (!validator.isEmail(email) || validator.isNull(password)) {
			console.log(getLogFormat(req) + '잘못된 요청 / meail: ' + email);
			sendError(res, '잘못된 요청입니다.');
			return;
		}

		User.find({ where: { email: email } }).success(function(user) {
			if (user) {
				if (user.authenticate(password)) {
					//logger.info(getLogFormat(req) + '유저 인증 성공 / user_id: ' + user.id);
					res.send(200, {
						result: 1,
						user_id: user.id,
						token: user.token
					});
				} else {
					console.log(getLogFormat(req) + '패스워드 불일치 / user_id: ' + user.id);
					sendError(res, '패스워드가 일치하지 않습니다. 다시 확인해 주세요.');
				}
			} else {
				console.log(getLogFormat(req) + '유저 정보 없음 / email: ' + email);
				sendError(res, '정보가 존재하지 않습니다. 회원가입 후 로그인 해주세요.');
			}
		}).error(function(err) {
			console.log(getLogFormat(req) + '유저 조회 실패 sequelize 오류 / email: ' + email);
			console.log(err);
			sendError(res, '서버 오류');
		});
	},

	getUser: function(req, res) {
		var userId = req.params.user_id;

		if (!validator.isNumeric(userId)) {
			console.log(getLogFormat(req) + '잘못된 요청 / user_id: ' + userId);
			sendError(res, '잘못된 요청입니다.');
			return;
		}

		User.find(userId).success(function(user) {
			if (user) {
				//logger.info(getLogFormat(req) + '유저 조회 성공 / user_id: ' + userId);
				res.send(200, {
					result: 1,
					user: {
						user_id: user.values.id,
						email: user.values.email,
						username: user.values.name,
						age: user.values.age,
						gender: user.values.gender,
						room_id: user.values.room_id
					}
				});
			} else {
				console.log(getLogFormat(req) + '유저 정보 없음 / user_id: ' + userId);
				sendError(res, '유저 정보가 없습니다.');
			}
		}).error(function(err) {
			console.log(getLogFormat(req) + '유저 조회 실패 Sequelize 오류 / user_id: ' + userId);
			console.log(err);
			sendError(res, '서버 오류');
		});
	},
	getUsersCount: function(req, res) {
		User.count().success(function(count) {
			//logger.info(getLogFormat(req) + '유저 수 조회 성공');
			res.send(200, {
				result: 1,
				count: count
			});
		}).error(function(err) {
			console.log(getLogFormat(req) + '유저 수 조회 실패 Sequelize 오류');
			console.log(err);
			sendError(res, '서버 오류');
		});
	},

	deleteUser: function(req, res) {
		return;
	},

	updateUser: function(req, res) {
		return;
	},

	// [TODO] 방을 지우면 방 일원의 room_id도 지우는 기능, 다음날 새벽 5시가 되면 모든 방 지우는 기능 구현 필요.
	// [TODO] size 증가하는 코드 구현해야 (sequelize.increase는 reload도 해줘야되고 훨씬 느려져서 지양. 동기화 또는 콜백적용 필요)
	createRoom: function(req, res) {
		var roomTitle = req.body.room_title;
		var hostId = parseInt(req.body.host_id);
		var region = req.body.region;// [TODO] 코드를 배정하는 걸로 변경해야.
		var description = req.body.description;
		var guests = eval(req.body.guests);
		//var userGuests = JSON.parse(req.body.user_guests);
		//var virtualGuests = eval(req.body.virtual_guests);

		var memberIds = [];
		var duplicateIds = [];

		if (validator.isNull(roomTitle)||!validator.isNumeric(hostId)||validator.isNull(region)) {
			//logger.info(getLogFormat(req) + '잘못된 요청 / host_id: ' + hostId);
			sendError(res, '잘못된 요청입니다.');
			return;
		}

		User.findOne({ where: { id: hostId } }).success(function(host) {
			/* host가 이미 다른 방에 속해있을 경우 */
			if (host.values.room_id) {
				sendError(res, '이미 다른 방에 속해있을 경우 방을 생성할 수 없습니다');
				return;
			}
			memberIds.push(host.values.id);
		}).error(function(err) {
			console.log(getLogFormat(req) + '유저 조회 실패 Sequelize 오류  / user_id: ' + hostId);
			console.log(err);
			sendError(res, '서버 오류');
		});

		/* 속도는 비슷. http 전송시 항이 적은 아래 방식 쓰자 그냥.
		   User.findAll({ where: { id: [userGuests] }, attributes: ['id', 'room_id'] }).success(function(users) {
		   _.each(users, function(user) {
		   if (user.values.room_id) {
		   duplicateIds.push(user.values.id);
		   return;
		   }
		   memberIds.push(user.values.id);
		   })
		   }).error(function(err) {
		   console.log(getLogFormat(req) + '동행 유저 조회 실패 Sequelize 오류');
		   console.log(err);
		   sendError(res, '선택하신 친구가 등록되지 않은 유저이거나 서버오류가 발생하였습니다.');
		   });

		   _.each(virtualGuests, function(vg) {
		   vg.is_virtual = true;
		   User.create(vg).success(function(guest) {
		   memberIds.push(guest.values.id);
		   }).error(function(err) {
		   console.log(getLogFormat(req) + '비회원 등록 실패 Sequelize 오류');
		   console.log(err);
		   sendError(res, '서버 오류');
		   });
		   });
		   */

		_.each(guests, function(guest, i) {
			if (guest.id) {
				// 회원일 경우
				User.findOne({ where: { id: guest.id }, attributes: ['id', 'room_id'] }).success(function(user) {
					if ((i + 1) == guests.length)
						process.emit('putDone', i);
					if (user.values.room_id) {
						duplicateIds.push(user.values.id);
						return;
					}
					memberIds.push(user.values.id);
				}).error(function(err) {
					console.log(getLogFormat(req) + '동행 유저 조회 실패 Sequelize 오류');
					consol.log(err);
					sendError(res, '선택하신 친구가 등록되지 않은 유저이거나 서버오류가 발생하였습니다.');
				});
			} else {
				// 비회원일 경우
				// { name: value, [age: value], gender: (m, f), is_virtual = true }
				// email, password, token = null ( is_virtual = true일 땐 null 가능 & is_virtual = null일 땐 불가능 한 칼럼들)
				//
				guest.is_virtual = true;
				User.create(guest).success(function(result) {
					if ((i + 1) == guests.length)
						process.emit('putDone', i);
					memberIds.push(result.values.id);
				}).error(function(err) {
					console.log(getLogFormat(req) + '비회원 등록 실패 Sequelize 오류');
					console.log(err);
					sendError(res, '서버 오류');
				});
			}
		});

		// 임시 떔빵... hook이나 promise를 쓰는건가 async를 쓰는건가..
		process.on('putDone', function(i) {
			var roomData = {
				title: roomTitle,
				region: region,
				description: description,
				host_id: hostId,
				size: memberIds.length
			}		

			Room.create(roomData).success(function(room) {
				if (memberIds) {
					room.setUsers(memberIds).success(function() {
						//logger.info(getLogFormat(req) + '방 생성 및 동행 등록 성공 / room_id: ' + room.values.id);
						res.send(200, {
							result: 1,
							room: {
								room_id: room.values.id,
								title: room.values.title,
								description: room.values.description,
								region: room.values.region,
								host_id: room.host_id,
								memberIds_ids: memberIds,
								size: room.values.size,
								duplicate_ids: duplicateIds
							}
						});
					}).error(function(err) {
						console.log(getLogFormat(req) + '동행 등록 실패 / room_id: ' + room.values.id);
						console.log(err);
						sendError(res, '친구 등록에 실패하였습니다.');
					});
				} else {
					res.send(200, {
						result: 1,
						room: {
							room_id: room.values.id,
							title: room.values.title,
							description: room.values.description,
							region: room.values.region,
							duplicate_ids: duplicateIds
						}
					});
				}
			}).error(function(err) {
				console.log(getLogFormat(req) + '방 생성 실패 / room_id: ' + room.values.id);
				console.log(err);
				sendError(res, '방 생성에 실패하였습니다.');
			});
		});
	},

	getRooms: function(req, res) {
		var region = req.params.region;

		Room.findAll({ 
			where: { is_end: null, region: region }, 
			attributes: ['id', 'title', 'photo_url'] }).success(function(rooms) {
				//logger.info(getLogFormat(req) + '방 목록 조회 성공 / region: ' + region);
				res.send(200, {
					result: 1,
					rooms: rooms,
					count: rooms.length
				});
			}).error(function(err) {
				console.log(getLogFormat(req) + '방 목록 조회 실패 Sequelize 오류');
				console.log(err);
				sendError(res, '서버 오류');
			});
	},

	getRoom: function(req, res) {
		var roomId = req.params.room_id;

		// [TODO] attributes에서 is_end, created updated 빼고 써넣기
		Room.findOne({
			where: { is_end: null, id: roomId },
			include: [{ model: User, attributes: ['id', 'name', 'age', 'gender', 'is_virtual'] }]
		}).success(function(result) {
			//logger.info(getLogFormat(req) + '방 조회 성공 / room_id: ' + roomId);
			res.send(200, {
				result: 1,
				room: result
			});
		}).error(function(err) {
			console.log(getLogFormat(req) + '방 조회 실패 Sequelize 오류');
			console.log(err);
			sendError(res, '서버 오류');
		});
	},

	getTotalRoomsCount: function(req, res) {
		Room.count({ where: { is_end: null } }).success(function(count) {
			res.send(200, {
				result: 1,
				count: count
			});
		}).error(function(err) {
			console.log(getLogFormat(req) + '방 갯수 조회 실패 Sequelize 오류');
			console.log(err);
			sendError(res, '서버 오류');
		});
	},

	deleteRoom: function(req, res) {
		return;
	},

	updateRoom: function(req, res) {
		return;
	},

	downloadFile: function(req, res) {
		return;
	},

	events: function(req, res) {
		return;
	},

	index: function(req, res) {
		res.render('index', { title: '만나고', layout: false });
	}
};
